<?php

/**
 * @file
 * Contains theme implementations.
 */

/**
 * FAPI theme for an individual text elements.
 */
function theme_addressfield($element) {
  // Adding JS to show you can override the default JS behaviour.
  drupal_add_js(drupal_get_path('module', 'addressfield') . '/addressfield.js', 'module');
  $output = '';
  $output .= '<div class="address-field clear-block">';
  $output .= theme('fieldset', $element['address']);
  $output .= '</div>';
  return $output;
}
