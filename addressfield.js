/**
 * This serves both as an example on how to override the default js and as an
 * easy way to show developers what is wrong with their setup.
 */
(function ($, Drupal, pca) {
    'use strict';

    /**
     * Removing default behaviour and displaying address in result div.
     */
    Drupal.behaviors.addressFieldCapturePlusResult = function (context) {
        var $addressWrapper = $('.capture_plus_address', context);
        var $searchBox = $('input.search-box.capture-plus', context);
        var fieldDisplay = $addressWrapper.attr('data-style');
        var $addressItems = $('.address-item', $addressWrapper);

        $.each($addressItems, function(index, item) {
            var $addressItem = $(item);
            var capturePlusField = $addressItem.attr('data-field');
            if (capturePlusField != undefined) {
                $addressItem.parent().hide();
            }
        });

        $searchBox.unbind('capturePlusCallback.default');
        $searchBox.bind('capturePlusCallback.capturePlusResult', function (event, response) {
            $.each($addressItems, function(index, item) {
                var $addressItem = $(item);
                var capturePlusField = $addressItem.attr('data-field');
                if (capturePlusField != undefined) {
                    $addressItem.parent().hide();
                    var fieldType = this.tagName.toLowerCase();
                    if (response[capturePlusField].length != 0) {
                        if (fieldType == 'input') {
                            $addressItem.val(response[capturePlusField]);
                        }
                        else {
                            $addressItem.html(response[capturePlusField]);
                        }

                        $addressItem.parent().fadeIn();
                    }
                    else {
                        $addressItem.val('');
                    }
                }
            });
            // For IE, clear the value so the placeholder polyfill returns.
            if ($.browser.msie && $.browser.version < 10) {
                $searchBox.val('').focus();
            }
        });
    };

    /**
     * Overriding default address formatter.
     *
     * @param response
     *   The Capture+ response.
     * @returns {string}
     *   The formatted address as html.
     */
    Drupal.theme.addressFieldCapturePlusLabel = function (response) {
        var output = '';
        var elements = ['Company', 'SubBuilding', 'BuildingName', 'BuildingNumber', 'Street', 'SecondaryStreet', 'City', 'PostalCode'];
        for (var i = 0; i < elements.length; i++) {
            if (response[elements[i]].length) {
                output += response[elements[i]] + "<br />";
            }
        }
        return output;
    };

})(jQuery, Drupal, pca);
